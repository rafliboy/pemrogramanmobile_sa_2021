package com.athallah.muhammad.activities

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.SearchView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.athallah.muhammad.R
import com.athallah.muhammad.adapter.MainAdapter
import com.athallah.muhammad.adapter.MainAdapter.onSelectData
import com.athallah.muhammad.adapter.SliderImageAdapter
import com.athallah.muhammad.model.ModelMain
import com.athallah.muhammad.model.ModelSlide
import com.athallah.muhammad.networking.ApiEndpoint
import com.athallah.muhammad.service.GetAddressIntentService
import com.google.android.gms.location.*
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.slider_imageview.*
import kotlinx.android.synthetic.main.toolbar_main.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
//mengimpor library java yang dibutuhkan
@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity(), onSelectData {
//membuat class MainActivity
    private var progressDialog: ProgressDialog? = null
    //membuat variabel progressDialog secara private dan mempunyai value Null
    private var mainAdapter: MainAdapter? = null
    //membuat variabel mainAdapter secara private dan mempunyai value Null
    private var modelMain: MutableList<ModelMain> = ArrayList()
    //membuat variabel modelMain secara private dan mempunyai value fungsi ArrayList()
    private val modelSlide: MutableList<ModelSlide> = ArrayList()
    //membuat variabel modelSlide secara private dan mempunyai value fungsi ArrayList()
    private var fusedLocationClient: FusedLocationProviderClient? = null
    //membuat variabel fusedLocationClient secara private dan mempunyai value Null
    private var addressResultReceiver: LocationAddressResultReceiver? = null
    //membuat variabel addressResultReceiver secara private dan mempunyai value Null
    private var currentLocation: Location? = null
    //membuat variabel currentLocation secara private dan mempunyai value Null
    private var locationCallback: LocationCallback? = null
    //membuat variabel locationCallback secara private dan mempunyai value Null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = Color.TRANSPARENT
        }
        //fungsi untuk membuat status bar menjadi transparan

        addressResultReceiver = LocationAddressResultReceiver(Handler())
        //membuat variabel addressResultReceiver dengan value fungsi LocationAddressResultReceiver(Handler())
        progressDialog = ProgressDialog(this)
        progressDialog?.setTitle("Mohon Tunggu")
        //mengatur judul text pada progress dialog
        progressDialog?.setCancelable(false)
        //mengatur progress dialog agar proses tidak bisa dibatalkan oleh user
        progressDialog?.setMessage("Sedang menampilkan data")
        //mengatur pesan text pada progress dialog

        searchBatik.setQueryHint("Cari Apa?")
        //place holder untuk kolom pencarian
        searchBatik.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                setSearchBatik(query)
                return false
            }
        //fungsi untuk mendengarkan dan membaca text type string pencarian dari user

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText == "") getAllBatik()
                return false
            }
        })

        val searchPlateId = searchBatik.context.resources
                .getIdentifier("android:id/search_plate", null, null)
        val searchPlate = searchBatik.findViewById<View>(searchPlateId)
        searchPlate?.setBackgroundColor(Color.TRANSPARENT)

        mainAdapter = MainAdapter(this, modelMain, this)

        rvAllBatik.setHasFixedSize(true)
        rvAllBatik.layoutManager = LinearLayoutManager(this)
        rvAllBatik.adapter = mainAdapter

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                currentLocation = locationResult.locations[0]
                getAddress()
            }
        }
        //membuat fungsi LocationCallback() untuk mengetahui lokasi user

        startLocationUpdates()
        //fungsi untuk mengupdate lokasi user

        //method get slide
        getSlideData()

        //method get data
        getAllBatik()
    }

    private fun setSearchBatik(query: String) {
    //membuat fungsi setSearchBatik secara private dengan paramater string
        progressDialog?.show()
        //fungsi untuk menampilkan progress dialog
        AndroidNetworking.get(ApiEndpoint.URL_SEARCH + query)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        try {
                            progressDialog?.dismiss()
                        //fungsi override untuk membaca request dari user dan mendapatkan objek

                            if (modelMain.isNotEmpty()) modelMain.clear()

                            val jsonArray = response.getJSONArray("hasil")
                            for (i in 0 until jsonArray.length()) {
                            //membuat perulangan dengan fungsi jsonArray.length()
                                val jsonObject = jsonArray.getJSONObject(i)
                                val dataApi = ModelMain()
                                dataApi.id = jsonObject.getInt("id")
                                dataApi.namaBatik = jsonObject.getString("nama_batik")
                                dataApi.daerahBatik = jsonObject.getString("daerah_batik")
                                dataApi.maknaBatik = jsonObject.getString("makna_batik")
                                dataApi.hargaRendah = jsonObject.getInt("harga_rendah")
                                dataApi.hargaTinggi = jsonObject.getInt("harga_tinggi")
                                dataApi.hitungView = jsonObject.getString("hitung_view")
                                dataApi.linkBatik = jsonObject.getString("link_batik")
                                //mendapatkan data dari dataApi
                                modelMain.add(dataApi)
                            }
                            mainAdapter?.notifyDataSetChanged()

                        } catch (e: JSONException) {
                            e.printStackTrace()
                            Toast.makeText(this@MainActivity, "Gagal menampilkan data!", Toast.LENGTH_SHORT).show()
                        }
                        //fungsi untuk menangkap dan menampilkan pesan error
                    }

                    override fun onError(anError: ANError) {
                        progressDialog?.dismiss()
                        Toast.makeText(this@MainActivity, "Tidak ada jaringan internet!", Toast.LENGTH_SHORT).show()
                        //fungsi untuk menangkap dan menampilkan pesan error
                    }
                })
    }

    private fun getSlideData() {
    //membuat fungsi getSlideData() secara private
        AndroidNetworking.get(ApiEndpoint.BASEURL_POPULAR)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    override fun onResponse(response: JSONObject) {
                        try {
                            val jsonArray = response.getJSONArray("hasil")
                            //fungsi onResponse dibuat secara override untuk mencoba mendapatkan response dari hasil
                            for (y in 0 until jsonArray.length()) {
                                val jsonObject = jsonArray.getJSONObject(y)
                                val mdlSlide = ModelSlide()
                                mdlSlide.namaBatik = jsonObject.getString("nama_batik")
                                //mendapatkan jsonObject type string dari "nama_batik"
                                mdlSlide.linkBatik = jsonObject.getString("link_batik")
                                //mendapatkan jsonObject type string dari "link_batik"
                                modelSlide.add(mdlSlide)
                                //menambahkan model slide
                            }
                            setImgSlide()
                            //mengatur gambar pada slide
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            Toast.makeText(this@MainActivity, "Gambar tidak ditemukan!", Toast.LENGTH_SHORT).show()
                        }
                        //menangkap dan menampilkan pesan error
                    }

                    override fun onError(anError: ANError) {
                        Toast.makeText(this@MainActivity, "Error Slide", Toast.LENGTH_SHORT).show()
                    }
                    //menangkap dan menampilkan pesan error
                })
    }


    private fun getAllBatik() {
    //membuat fungsi secara private
        Log.d("debug", "get all batik")
        progressDialog?.show()
        //menampilkan pesan dari progress dialog
        AndroidNetworking.get(ApiEndpoint.BASEURL_ALL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.d("debug", "all batik response : $response")
                        try {
                            progressDialog?.dismiss()
                        //mencoba menolak respon dari progress dialog
                            val jsonArray = response.getJSONArray("hasil")
                            for (i in 0 until jsonArray.length()) {
                            //melakukan perulangan pada jsonArray dengan fungsi length()
                                val jsonObject = jsonArray.getJSONObject(i)
                                val dataApi = ModelMain()
                                dataApi.id = jsonObject.getInt("id")
                                dataApi.namaBatik = jsonObject.getString("nama_batik")
                                dataApi.daerahBatik = jsonObject.getString("daerah_batik")
                                dataApi.maknaBatik = jsonObject.getString("makna_batik")
                                dataApi.hargaRendah = jsonObject.getInt("harga_rendah")
                                dataApi.hargaTinggi = jsonObject.getInt("harga_tinggi")
                                dataApi.hitungView = jsonObject.getString("hitung_view")
                                dataApi.linkBatik = jsonObject.getString("link_batik")
                                //mendapatkan data
                                modelMain.add(dataApi)
                            }
                            Log.d("debug", "data size ${modelMain.size}")
                            // notify adapter
                            mainAdapter?.notifyDataSetChanged()

                        } catch (e: JSONException) {
                            Log.e("error", "error ${e.localizedMessage}")
                            Toast.makeText(this@MainActivity, "Gagal menampilkan data!", Toast.LENGTH_SHORT).show()
                        }
                        //menangkap dan menampilkan pesan error
                    }

                    override fun onError(anError: ANError) {
                        Log.e("error", "error request: ${anError.localizedMessage}")
                        progressDialog?.dismiss()
                        Toast.makeText(this@MainActivity, "Tidak ada jaringan internet!", Toast.LENGTH_SHORT).show()
                    }
                    //menangkap dan menampilkan pesan error
                })
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private fun setImgSlide() {
    //membuat fungsi secara private
        val sliderImageAdapter = SliderImageAdapter(this, modelSlide)
        sliderImageAdapter.count = modelSlide.size
        imgSlider.setSliderAdapter(sliderImageAdapter)
        imgSlider.setIndicatorAnimation(IndicatorAnimations.DROP)
        imgSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        imgSlider.indicatorSelectedColor = Color.WHITE
        imgSlider.indicatorUnselectedColor = getColor(R.color.colorAccent)
        imgSlider.startAutoCycle()
        imgSlider.setOnIndicatorClickListener { position -> imgSlider.currentPagePosition = position }
        //mengatur bentuk, warna dan perilaku gambar pada slider
    }

    override fun onSelected(modelMain: ModelMain) {
        val intent = Intent(this@MainActivity, DetailBatikActivity::class.java)
        intent.putExtra("detailBatik", modelMain)
        startActivity(intent)
        //fungsi untuk memulai aktivitas dari user jika user memilih item 
    }

    private fun startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE)
                    //membuat fungsi untuk mendapatkan perizinan lokasi dari user
                    //melakukan perulangan
        } else {
            val locationRequest = LocationRequest()
            locationRequest.interval = 2000
            locationRequest.fastestInterval = 1000
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            fusedLocationClient!!.requestLocationUpdates(locationRequest, locationCallback, null)
            //membuat hasil lain jika perulangan ber nilai False
        }
    }

    private fun getAddress() {
        if (!Geocoder.isPresent()) {
            Toast.makeText(this@MainActivity, "Can't find current address, ", Toast.LENGTH_SHORT).show()
            return
        }
        val intent = Intent(this, GetAddressIntentService::class.java)
        intent.putExtra("add_receiver", addressResultReceiver)
        intent.putExtra("add_location", currentLocation)
        startService(intent)
        //mendapatkan lokasi dari user
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates()
            } else {
                Toast.makeText(this, "Location permission not granted", Toast.LENGTH_SHORT).show()
            }
        }
        //fungsi untuk menampilkan pesan izin lokasi tidak diberikan jika user tidak memberi izin terhadap aplikasi
    }

    private inner class LocationAddressResultReceiver internal constructor(handler: Handler?) : ResultReceiver(handler) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
            if (resultCode == 0) {
                getAddress()
            }
            if (resultCode == 1) {
                Toast.makeText(this@MainActivity, "Address not found, ", Toast.LENGTH_SHORT).show()
            }
            val currentAdd = resultData.getString("address_result")
            showResults(currentAdd)
            //fungsi untuk menampilkan pesan alamat tidak ditemukan jika alamat user tidak bisa didapatkan
        }
    }

    private fun showResults(currentAdd: String?) {
        tvLocation.text = currentAdd
        //menampilkan hasil lokasi user
    }

    override fun onResume() {
        super.onResume()
        startLocationUpdates()
        //melakukan update lokasi user
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient?.removeLocationUpdates(locationCallback)
        //menghapus lokasi update pada user jika lokasi user sudah akurat
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 2
        fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
            val window = activity.window
            val winParams = window.attributes
            if (on) {
                winParams.flags = winParams.flags or bits
            } else {
                winParams.flags = winParams.flags and bits.inv()
            }
            window.attributes = winParams
        }
    }
}
