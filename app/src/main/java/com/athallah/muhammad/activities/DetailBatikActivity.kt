package com.athallah.muhammad.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.athallah.muhammad.R
import com.athallah.muhammad.model.ModelMain
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.activity_detail_batik.*
import java.text.NumberFormat
import java.util.*
//meimport library java yang ingin digunakan

class DetailBatikActivity : AppCompatActivity() {
//membuat class DetailBatikActivity

    var NamaBatik: String? = null
    //membuat variabel NamaBatik type string dengan value Null.
    var AsalBatik: String? = null
    //membuat variabel AsalBatik type string dengan value Null.
    var DescBatik: String? = null
    //membuat variabel DescBatik type string dengan value Null.
    var Cover: String? = null
    //membuat variabel Cover type string dengan value Null.
    var HargaRendah = 0
    //membuat variabel HargaRendah type integer dengan value 0.
    var HargaTinggi = 0
    //membuat variabel HargaTinggi type integer dengan value 0.
    var modelMain: ModelMain? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Fungsi akan dipanggil ketika tombol Kirim diketuk.
        setContentView(R.layout.activity_detail_batik)
        //mengatur content view terhadap layout activity_detail_batik.
    
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = Color.TRANSPARENT
        }
        //membuat statusbar menjadi transparan.
        
        toolbar.setTitle("")
        //mengatur judul terhadap toolbar.
        setSupportActionBar(toolbar)
        assert(supportActionBar != null)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        modelMain = intent.getSerializableExtra("detailBatik") as ModelMain
        if (modelMain != null) {
            NamaBatik = modelMain?.namaBatik
            AsalBatik = modelMain?.daerahBatik
            HargaRendah = modelMain!!.hargaRendah
            HargaTinggi = modelMain!!.hargaTinggi
            DescBatik = modelMain?.maknaBatik
            Cover = modelMain?.linkBatik
            //model untuk menampilkan data.

            val localeID = Locale("in", "ID")
            val formatRupiah = NumberFormat.getCurrencyInstance(localeID)

            tvTitle.setText("Nama Batik : $NamaBatik")
            //mengatur judul Nama Batik terhadap $NamaBatik
            tvAsalBatik.setText("Asal Batik : $AsalBatik")
            //mengatur judul Asal Batik terhadap $AsalBatik
            tvHargaRendah.setText(formatRupiah.format(HargaRendah.toDouble()))
            //mengatur format nilai angka pada harga rendah ke type Double
            tvHargaTinggi.setText(formatRupiah.format(HargaTinggi.toDouble()))
            //mengatur format nilai angka pada harga tinggi ke type Double
            tvDescBatik.setText(DescBatik)
            //mengatur text terhadap variabel DescBatik

            Glide.with(this)
                    .load(Cover)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgCover)
            //fungsi glade untuk menyesuaikan memori dan perilaku cache disk
        }

        fabShare.setOnClickListener(View.OnClickListener {
        //digunakan untuk mendengarkan klik dari user
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            val subject = modelMain?.namaBatik
            val description = modelMain?.maknaBatik
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
            shareIntent.putExtra(Intent.EXTRA_TEXT, subject + "\n\n" + description);
            startActivity(Intent.createChooser(shareIntent, "Bagikan dengan :"))
             //digunakan untuk mendengarkan klik dari user lalu memanggil shareintent.
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
    //fungsi ini akan dipanggil jika menu dipilih oleh user.

    companion object {
        fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
            val window = activity.window
            val winParams = window.attributes
            if (on) {
                winParams.flags = winParams.flags or bits
            } else {
                winParams.flags = winParams.flags and bits.inv()
            }
            window.attributes = winParams
        }
    }
}