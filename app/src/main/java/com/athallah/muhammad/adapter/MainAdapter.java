package com.athallah.muhammad.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.athallah.muhammad.R;
import com.athallah.muhammad.model.ModelMain;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
//mengimpor library java yang dibutuhkan



public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
//membuat public class MainAdapter yang mewarisi dari RecyclerView.Adapter .
    private List<ModelMain> items;
    //membuat fungsi List secara private.
    private MainAdapter.onSelectData onSelectData;
    //membuat fungsi MainAdapter.onSelectData secara private.
    private Context mContext;
    //membuat fungsiContext secara private.

    public interface onSelectData {
        void onSelected(ModelMain modelMain);
    }
    //membuat interface onSelectData.

    public MainAdapter(Context context, List<ModelMain> items, MainAdapter.onSelectData xSelectData) {
        this.mContext = context;
        this.items = items;
        this.onSelectData = xSelectData;
        //memanggil fungsi fungsi yang telah dibuat.
    }

    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_batik, parent, false);
        return new MainAdapter.ViewHolder(view);
        //mengembalikan nilai dari fungsi MainAdapter.
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MainAdapter.ViewHolder holder, int position) {
        final ModelMain data = items.get(position);
        //memebuat variabel final untuk mendapatkan posisi dari item

        holder.tvTitle.setText(data.getNamaBatik());
        //mengatur judul text dengan mendapatkan data dari NamaBatik
        holder.tvDilihat.setText("Dilihat " +data.getHitungView() + " orang");
        //mengatur text untuk fungsi tvDilihat() dan mendapatkan hasil dari getHitungView() lalu ditampilkan
        holder.tvDesc.setText(data.getMaknaBatik());
        //mengatur text dengan mendapatkan data dari MaknaBatik

        Glide.with(mContext)
                .load(data.getLinkBatik())
                .apply(new RequestOptions())
                .into(holder.imgBatik);

        holder.cvBatik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectData.onSelected(data);
            }
            //fungsi untuk mendengarkan data yang di klik oleh user
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
        //fungsi untuk mengembalikan nilai fungsi items.size()
    }

    //Class Holder
    class ViewHolder extends RecyclerView.ViewHolder {
//membuat class ViewHolder yang mewarisi RecyclerView.ViewHolder
        public CardView cvBatik;
        public ImageView imgBatik;
        public TextView tvTitle;
        public TextView tvDilihat;
        public TextView tvDesc;
        //membuat fungsi card,image dan text view secara public.

        public ViewHolder(View itemView) {
            super(itemView);
            cvBatik = itemView.findViewById(R.id.cvBatik);
            imgBatik = itemView.findViewById(R.id.imgBatik);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDilihat = itemView.findViewById(R.id.tvDilihat);
            tvDesc = itemView.findViewById(R.id.tvDesc);
        //fungsi untuk menemukan dan menampilkan data berdasarkan Id
        }
    }
}