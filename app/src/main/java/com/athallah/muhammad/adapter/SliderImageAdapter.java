package com.athallah.muhammad.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.athallah.muhammad.R;
import com.athallah.muhammad.model.ModelSlide;
import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;
//mengimport library java yang dibutuhkan


public class SliderImageAdapter extends SliderViewAdapter<SliderImageAdapter.SliderAdapterVH> {
//membuat class SliderImageAdapter yang mewarisi SliderViewAdapter
    private Context mContext;
    private List<ModelSlide> mSliderItems;
    private int mCount;
    //membuat fungsi secara private.

    public SliderImageAdapter(Context mContext, List<ModelSlide> mSliderItems) {
        this.mContext = mContext;
        this.mSliderItems = mSliderItems;
        //fungsi ini digunakan untuk memanggil data fungsi mContext dan mSliderItems
    }

    public void setCount(int count) {
        this.mCount = count;
        //fungsi untuk mengatur perhitungan data integer terhadap fungsi mCount
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_slider, null);
        return new SliderAdapterVH(view);
        //fungsi untuk mengembalikan nilai baru dari SliderAdapterVH() 
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
    //membuat fungsi nBindViewHolder dengan parameter
        ModelSlide sliderItem = mSliderItems.get(position);
        //variabel dibuat untuk mendapatkan posisi dari item slider

        viewHolder.tvDescSlider.setText(sliderItem.getNamaBatik());
        viewHolder.tvDescSlider.setTextSize(12);
        viewHolder.tvDescSlider.setTextColor(Color.WHITE);
        Glide.with(viewHolder.itemView)
                .load(sliderItem.getLinkBatik())
                .fitCenter()
                .into(viewHolder.imgAutoSlider);
        //fungsi untuk mengatur perilaku text pada DescSlider

        /*viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
    }

    public int getCount() {
        return mCount;
        //mengembalikan nilai fungsi Mcount
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
//membuat fungsi SliderAdapterVH yang mewarisi SliderViewAdapter
        View itemView;
        ImageView imgAutoSlider;
        TextView tvDescSlider;
        //mengatur image dan text view terhadap slider

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imgAutoSlider = itemView.findViewById(R.id.imgAutoSlider);
            tvDescSlider = itemView.findViewById(R.id.tvDescSlider);
            this.itemView = itemView;
            //fungsi untuk menemukan item atau data berdasarkan Id.
        }
    }
}
