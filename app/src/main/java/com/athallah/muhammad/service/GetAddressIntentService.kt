package com.athallah.muhammad.service

import android.app.IntentService
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.ResultReceiver
import java.util.*
//mengimport library java yang dibutuhkan


class GetAddressIntentService : IntentService(IDENTIFIER) {
//membuat class GetAddressIntentService.

    private var addressResultReceiver: ResultReceiver? = null
    //membuat fungsi addressResultReceiver secara private dengan value Null.

    
    override fun onHandleIntent(intent: Intent?) {
    //menangani permintaan alamat
        var msg = ""
        addressResultReceiver = intent!!.getParcelableExtra("add_receiver")
        if (addressResultReceiver == null) {
            return
        }
        val location = intent.getParcelableExtra<Location>("add_location")
        //mendapatkan hasil dari intent
        if (location == null) {
            msg = "lokasi tidak ditemukan"
            sendResultsToReceiver(0, msg)
            return
        }
        //mengirim error lokasi tidak ditemukan ke user
        val geocoder = Geocoder(this, Locale.getDefault())
        var addresses: List<Address>? = null
        try {
            addresses = geocoder.getFromLocation(
                    location.latitude,
                    location.longitude,
                    1)
        } catch (ignored: Exception) {
        }
        if (addresses == null || addresses.size == 0) {
            msg = "tidak ditemukan dilokasi tersebut"
            sendResultsToReceiver(1, msg)
        } else {
            val address = addresses[0]
            val addressDetails = StringBuffer()
            addressDetails.append(address.subAdminArea)
            //addressDetails.append("\n");

            /*addressDetails.append(address.getThoroughfare());
            addressDetails.append("\n");

            addressDetails.append("Locality: ");
            addressDetails.append(address.getLocality());
            addressDetails.append("\n");

            addressDetails.append("County: ");
            addressDetails.append(address.getFeatureName());
            addressDetails.append("\n");

            addressDetails.append("State: ");
            addressDetails.append(address.getAdminArea());
            addressDetails.append("\n");

            addressDetails.append("Country: ");
            addressDetails.append(address.getCountryName());
            addressDetails.append("\n");

            addressDetails.append("Postal Code: ");
            addressDetails.append(address.getPostaalCode());
            addressDetails.append("\n");*/
            sendResultsToReceiver(2, addressDetails.toString())
        }
    }

    private fun sendResultsToReceiver(resultCode: Int, message: String) {
        val bundle = Bundle()
        bundle.putString("address_result", message)
        addressResultReceiver?.send(resultCode, bundle)
    }

    companion object {
        private const val IDENTIFIER = "GetAddressIntentService"
    }
}
//untuk mengirim hasil kepada penerima di sumber aktivitas