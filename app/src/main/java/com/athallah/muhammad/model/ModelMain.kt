package com.athallah.muhammad.model

import java.io.Serializable
//mengimport library java yang dibutuhkan.
class ModelMain : Serializable {
//membuat class ModelMain.
    var id = 0
    //membuat variabel id type integer dengan value 0.
    var namaBatik: String? = null
    //membuat variabel namaBatik type string value Null.
    var daerahBatik: String? = null
    //membuat variabel daerahBatik type string value Null.
    var maknaBatik: String? = null
    //membuat variabel maknaBatik type string value Null.
    var hargaRendah = 0
    //membuat variabel hargaRendah type integer dengan value 0.
    var hargaTinggi = 0
    //membuat variabel hargaTinggi type integer dengan value 0.
    var hitungView: String? = null
    //membuat variabel hitungView type string dengan value Null.
    var linkBatik: String? = null
    //membuat variabel linkBatik type string dengan value Null.

}